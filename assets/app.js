/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import 'bootstrap/dist/js/bootstrap';
import * as mdb from 'mdb-ui-kit'; // lib
import './styles/app.scss';

// start the Stimulus application
import './bootstrap';
