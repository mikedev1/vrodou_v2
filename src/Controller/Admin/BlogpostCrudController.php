<?php

namespace App\Controller\Admin;

use App\Entity\Blogpost;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;

class BlogpostCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Blogpost::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            ImageField::new('imageName')->setBasePath('/images/blogposts')->onlyOnIndex(),
            TextField::new('imageFile')->setFormType(VichImageType::class),
            TextField::new('name'),
            AssociationField::new('category'),
            DateField::new('createdAt')
        ];
    }
    
}
