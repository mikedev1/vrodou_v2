<?php
namespace App\Components;

use App\Repository\BlogpostRepository;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent('blogpost_search')]
class BlogpostSearchComponent
{
    use DefaultActionTrait;

    #[LiveProp(writable: true)]
    public string $query = '';

    public function __construct(private BlogpostRepository $blogpostRepository)
    {
    }

    public function getBlogposts(): array
    {
        // example method that returns an array of Products
        return $this->blogpostRepository->search($this->query);
    }
}